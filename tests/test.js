const assert = require('assert');
const fs = require('fs');
const app = require('../app.js');
const config = require('../../config.json');
const esi = require("eve-swagger");
const esi2 = esi({
    service: "https://esi.tech.ccp.is",
    source: "tranquility",
    agent: "bombers bar motd reader",
    language: "en-us",
    timeout: 6000,
    minTime: 0,
    maxConcurrent: 0
});

describe('BB Motd Reader', function() {
    describe('esi api', function() {
        it('Checking if the server is online', function() {
            esi2.status().then(data =>{
                assert.notStrictEqual(data.players, 0,"The server is offline")})
        });
    });
    describe('getKillmailDetails Method', function() {
        it('Checking if the getKillmailDetails() method returns correct data.', function() {
            app.getKillmailDetails(69184816).then(killmail => {
                assert.strictEqual(killmail.ship, 'Scorpion');
                assert.strictEqual(killmail.totalValue, 197616861.81);
                assert.strictEqual(killmail.victim, 'TonTornado');
                assert.strictEqual(killmail.killID, 69184816);
                assert.strictEqual(killmail.shipTypeID, 640);
            });
        });
    });
    describe('getBBMotd Method', function() {
        it('Checking if the getBBMotd() Method works correctly.', function() {
        app.getBBMotd(config)
        .then(motd => {
            assert.notStrictEqual(motd, {},"The motd should not be empty !")
            assert.notStrictEqual(motd.text, {},"The motd.text should not be empty !")
            assert.notStrictEqual(motd.upcomingFleets, {},"The motd.upcomingFleets should not be empty !")
        })
        .catch(err => {console.log(err)});        
        })
    })
});